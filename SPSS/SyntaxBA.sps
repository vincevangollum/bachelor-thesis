﻿* Encoding: UTF-8.
COMPUTE filter_$=(F518_SUF < 99998).
VARIABLE LABELS filter_$ 'F518_SUF < 99998 (FILTER)'.
VALUE LABELS filter_$ 0 'Not Selected' 1 'Selected'.
FORMATS filter_$ (f1.0).
FILTER BY filter_$.
EXECUTE.
COMPUTE LNEinkommen=ln(F518_SUF).
EXECUTE.
RECODE Max1202 (1=1) (9=SYSMIS) (ELSE=0) INTO Max1202ohneBerufsabschluss. 
VARIABLE LABELS  Max1202ohneBerufsabschluss 'Kein Berufsabschluss'. 
EXECUTE.  
RECODE Max1202 (9=SYSMIS) (2=1) (ELSE=0) INTO Max1202schulischebetrieblicheAusbildung. 
VARIABLE LABELS  Max1202schulischebetrieblicheAusbildung 'Schulische oder betriebliche Ausbildung'. 
EXECUTE.  
RECODE Max1202 (9=SYSMIS) (3=1) (ELSE=0) INTO Max1202Aufstiegsfortbildung. 
VARIABLE LABELS  Max1202Aufstiegsfortbildung 'Aufstiegsfortbildung '. 
EXECUTE.  
RECODE Max1202 (9=SYSMIS) (4=1) (ELSE=0) INTO Max1202Akademiker. 
VARIABLE LABELS  Max1202Akademiker 'Akademischer Abschluss'. 
EXECUTE.
RECODE F1300 (9=SYSMIS) (2=1) (ELSE=0) INTO F1300JaMehrere. 
VARIABLE LABELS  F1300JaMehrere 'Besuch mehrerer Fortbildungen in den letzten 2 Jahren'. 
EXECUTE.  
RECODE F1300 (9=SYSMIS) (1=1) (ELSE=0) INTO F1300JaEine. 
VARIABLE LABELS  F1300JaEine 'Besuch einer Fortbildungen in den letzten 2 Jahren'. 
EXECUTE.  
RECODE F1300 (9=SYSMIS) (3=1) (ELSE=0) INTO F1300keine. 
VARIABLE LABELS  F1300keine 'Besuch keiner Fortbildung in den letzten 2 Jahren'. 
EXECUTE.
RECODE F1600 (9=SYSMIS) (1=1) (ELSE=0) INTO F1600Verheiratet. 
VARIABLE LABELS  F1600Verheiratet 'Familienstand verheiratet'. 
EXECUTE.
RECODE F515 (99=SYSMIS) (1=1) (ELSE=0) INTO F515_1Person. 
VARIABLE LABELS  F515_1Person '1 Person im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (2=1) (ELSE=0) INTO F515_2Personen. 
VARIABLE LABELS  F515_2Personen '2 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (3=1) (ELSE=0) INTO F515_3bis4Personen. 
VARIABLE LABELS  F515_3bis4Personen '3 bis 4 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (4=1) (ELSE=0) INTO F515_5bis9Personen. 
VARIABLE LABELS  F515_5bis9Personen '5 bis 9 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (5=1) (ELSE=0) INTO F515_10bis19Personen. 
VARIABLE LABELS  F515_10bis19Personen '10 bis 19 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (6=1) (ELSE=0) INTO F515_20bis49Personen. 
VARIABLE LABELS  F515_20bis49Personen '20 bis 49 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (7=1) (ELSE=0) INTO F515_50bis99Personen. 
VARIABLE LABELS  F515_50bis99Personen '50 bis 99 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (8=1) (ELSE=0) INTO F515_100bis249Personen. 
VARIABLE LABELS  F515_100bis249Personen '100 bis 249 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (9=1) (ELSE=0) INTO F515_250bis499Personen. 
VARIABLE LABELS  F515_250bis499Personen '250 bis 499 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (10=1) (ELSE=0) INTO F515_500bis999Personen. 
VARIABLE LABELS  F515_500bis999Personen '500 bis 999 Personen im Unternehmen'. 
EXECUTE.  
RECODE F515 (99=SYSMIS) (11=1) (ELSE=0) INTO F515_1000undmehrPersonen. 
VARIABLE LABELS  F515_1000undmehrPersonen '1000 oder mehr Personen im Unternehmen'. 
EXECUTE.
COMPUTE F515_1bis249=F515_1Person+F515_2Personen+F515_3bis4Personen+F515_5bis9Personen+ 
    F515_10bis19Personen+F515_20bis49Personen+F515_50bis99Personen+F515_100bis249Personen. 
EXECUTE. 
COMPUTE F515_250bis999=F515_250bis499Personen + F515_500bis999Personen. 
EXECUTE.
RECODE F510 (9999=SYSMIS). 
EXECUTE.
COMPUTE Betriebszugehörigkeit=2012-F510. 
EXECUTE.
RECODE F518_SUF (99998=SYSMIS) (99999=SYSMIS). 
EXECUTE. 
RECODE Zpalter (9999=SYSMIS). 
EXECUTE. 
RECODE S1 (2=0). 
EXECUTE. 
RECODE F200 (97=SYSMIS) (99=SYSMIS). 
EXECUTE.
RECODE F1300 (9=SYSMIS). 
EXECUTE.
FREQUENCIES VARIABLES=Zpalter S1 F1607_01 Max1202 F1600Verheiratet F1300 F200 F515_1bis249 
    F515_250bis999 F515_1000undmehrPersonen Betriebszugehörigkeit 
  /STATISTICS=MEAN MEDIAN MODE 
  /BARCHART PERCENT 
  /ORDER=ANALYSIS.
FREQUENCIES VARIABLES=LNEinkommen F518_SUF 
  /STATISTICS=MEAN MEDIAN 
  /HISTOGRAM NORMAL 
  /ORDER=ANALYSIS.
REGRESSION 
  /DESCRIPTIVES MEAN STDDEV CORR SIG N 
  /MISSING LISTWISE 
  /STATISTICS COEFF OUTS CI(95) BCOV R ANOVA COLLIN TOL 
  /CRITERIA=PIN(.05) POUT(.10) 
  /NOORIGIN 
  /DEPENDENT LNEinkommen 
  /METHOD=ENTER Max1202ohneBerufsabschluss Max1202Aufstiegsfortbildung Max1202Akademiker 
    F1300JaMehrere F1300keine 
  /METHOD=ENTER Zpalter S1 F1600Verheiratet F1607_01 
  /METHOD=ENTER F200 F515_250bis999 F515_1000undmehrPersonen Betriebszugehörigkeit 
  /SCATTERPLOT=(*ZRESID ,*ZPRED) 
  /RESIDUALS HISTOGRAM(ZRESID) NORMPROB(ZRESID).

